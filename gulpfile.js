var gulp = require('gulp');
var browserSync = require('browser-sync').create();

var pug = require('gulp-pug');
var sass = require('gulp-sass');
var ts = require('gulp-typescript');

var webpackStream = require('webpack-stream');
var webpack = webpackStream.webpack;
var notify = require('gulp-notify');
var plumber = require('gulp-plumber');

var autoprefixer = require('gulp-autoprefixer');
var removeComments = require('gulp-strip-css-comments');
var imagemin = require('gulp-imagemin');

var named = require('vinyl-named');
var rimraf = require('rimraf');
var rename = require('gulp-rename');
var runSequence = require('run-sequence');

var path = require('./tasks/config');
var ngrok = require('ngrok');

gulp.task('browser-sync', function () {
	browserSync.init({
		server: {baseDir: 'build'}
	});
});

gulp.task('sass-build', function () {
	return gulp.src(path.src.sass)
		.pipe(plumber({
			errorHandler: notify.onError(function (err) {
				return {
					title: 'Error sass',
					message: err.message
				}
			})
		}))
		.pipe(sass({
			// outputStyle: 'compressed'
		}))
		.pipe(autoprefixer({
			browsers: ['last 5 versions'],
			cascade: true
		}))
		.pipe(removeComments())
		.pipe(gulp.dest(path.build.css))
		.pipe(browserSync.stream());
});

gulp.task('pug-build', function () {
	return gulp.src(path.src.pug)
		.pipe(plumber({
			errorHandler: notify.onError(function (err) {
				return {
					title: 'Error pug',
					message: err.message
				}
			})
		}))
		.pipe(pug({
			pretty: true
		}))
		.pipe(rename('index.html'))
		.pipe(gulp.dest(path.build.html))
		.pipe(browserSync.stream());
});

gulp.task('ts-build', function () {
	var config = {
		devtool: 'source-map',
		resolve: {
			modules: ['node_modules'],
			extensions: ['.tsx', '.ts', '.js']
		},
		resolveLoader: {
			modules: ['node_modules'],
			moduleExtensions: ['-loader'],
			extensions: ['.js']
		},
		module: {
			loaders: [{
				test: /\.tsx?$/,
				exclude: /(node_modules|bower_components)/,
				loader: 'ts'
			}
			],
		},
		plugins: [
			// new webpack.optimize.UglifyJsPlugin({
			// 	compress: {
			// 		warnings: false,
			// 		drop_console: true,
			// 		unsafe: true
			// 	}
			// }),
			new webpack.NoEmitOnErrorsPlugin()
		],
		externals: {
			"react": "React",
			"react-dom": "ReactDOM"
		},
	};
	return gulp.src(path.src.ts)
		.pipe(named())
		.pipe(plumber({
			errorHandler: notify.onError(function (err) {
				return {
					title: 'Error ts',
					message: err.message
				}
			})
		}))
		.pipe(webpackStream(config))
		.pipe(gulp.dest(path.build.js))
		.pipe(browserSync.stream());
});

gulp.task('copy-img', function () {
	gulp.src(path.src.img)
		.pipe(gulp.dest(path.build.img))
		.pipe(browserSync.stream());
});

gulp.task('compress-img', function () {
	gulp.src(path.src.img)
		.pipe(imagemin({
			optimizationLevel: 3,
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			interlaced: true
		}))
		.pipe(gulp.dest(path.build.img))
		.pipe(browserSync.stream());
});

gulp.task('copy-lib', function () {
	gulp.src('bower_components/jquery/dist/jquery.js')
		.pipe(gulp.dest(path.build.js));
	gulp.src('bower_components/react/react.development.js')
		.pipe(gulp.dest(path.build.js));
	gulp.src('bower_components/react/react-dom.development.js')
		.pipe(gulp.dest(path.build.js));
});

gulp.task('ngrok', function () {
	ngrok.connect({
		proto: 'http',
		addr: 3000
	},function (err, url) {
		console.log(url);
		console.log(err);
	});
});

gulp.task('watch-prodaction', function () {
	watch('prodaction');
});

gulp.task('watch-default', function () {
	watch('default');
});

gulp.task('clean', function (cb) {
	rimraf(path.clean, cb);
});

gulp.task('build', function (cb) {
	runSequence('clean', 'copy-lib', 'pug-build', 'sass-build', 'ts-build', cb);
});

gulp.task('prodaction', function (cb) {
	runSequence('build', 'compress-img', ['browser-sync', 'watch-prodaction'], cb);
});

gulp.task('default', function (cb) {
	runSequence('build', 'copy-img', ['browser-sync', 'watch-default'], cb);
});

function watch(str) {
	gulp.watch(path.watch.pug, ['pug-build']);
	gulp.watch(path.watch.sass, ['sass-build']);
	gulp.watch(path.watch.ts, ['ts-build']);
	switch (str) {
		case 'prodaction': {
			gulp.watch(path.watch.img, ['compress-img']);
			break;
		}
		case 'default': {
			gulp.watch(path.watch.img, ['copy-img']);
			break;
		}
	}
}