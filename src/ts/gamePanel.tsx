import * as React from 'react';

export default class GamePanel extends React.Component<PropsGamePanel, any> {
	constructor(props: any) {
		super(props);
	}

	public render() {
		const {timer, score} = this.props;
		return (
			<div className='game-panel'>
				<div className='timer'>
					{timer}
				</div>
				<div className='score'>
					Score {score}
				</div>
			</div>
		)
	}
}