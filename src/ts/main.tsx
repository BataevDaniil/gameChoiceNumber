import * as React from 'react';
import {render} from 'react-dom';

import Number from './number';
import GamePanel from './gamePanel';
import Equal from './equal';

import Timer from './timer'

class Game extends React.Component<object, StateGame> {
	public defaultTime:SMH = {second: 0, minute: 1, hour: 0};
	public timer: Timer;
	public rangeNum:{min:number, max:number} = {
		min: -100,
		max: 100
	};

	public state = {
		firstNum: Game.getRandomInt(this.rangeNum.min, this.rangeNum.max),
		secondNum: Game.getRandomInt(this.rangeNum.min, this.rangeNum.max),
		timer: Game.SMHToString(this.defaultTime),
		score: 0
	};

	constructor(props: any) {
		super(props);
		this.timer = new Timer(this.defaultTime, this.callTimer, false);
	}

	public render() {
		return (
			<div>
				<GamePanel timer={this.state.timer} score={this.state.score}/>
				<Number num={this.state.firstNum} onClick={this.clickFirstNum}/>
				<Equal onClick={this.clickCenter}/>
				<Number num={this.state.secondNum} onClick={this.clickSecondNum}/>
				<div className='question'>Which one is larger?</div>
			</div>
		)
	}

	public clickFirstNum = () => {
		if (this.state.firstNum > this.state.secondNum) {
			console.log('first num yes');
			this.ok();
		}
		else {
			console.log('first num no');
			this.fail();
		}
		this.generateNumber();
	};

	public clickCenter = () => {
		if (this.state.firstNum = this.state.secondNum) {
			console.log('center yes');
			this.ok();
		}
		else {
			console.log('center no');
			this.fail();
		}
		this.generateNumber();

	};

	public clickSecondNum = () => {
		if (this.state.firstNum < this.state.secondNum) {
			console.log('second num yes');
			this.ok();
		}
		else {
			console.log('second num no');
			this.fail();
		}
		this.generateNumber();
	};

	public generateNumber = () => {
		this.setState({
			firstNum: Game.getRandomInt(this.rangeNum.min, this.rangeNum.max),
			secondNum: Game.getRandomInt(this.rangeNum.min, this.rangeNum.max)
		});
		console.log(this.state);
	};

	public ok():void {
		this.setState({
			score: this.state.score + 1
		})

	}

	public fail():void {
		this.callTimer(this.timer.removeTime({second: 5, minute: 0, hour: 0}));
		this.setState({
			score: this.state.score - 1
		})

	}

	public callTimer = (time: SMH)  => {
		this.setState({
			timer: Game.SMHToString(time)
		});
		console.log(Game.SMHToString(time));
	};

	public static SMHToString(time: SMH):string {
		return `${time.minute}:${time.second}`
	}

	public static getRandomInt(min: number, max: number):number {
		return Math.floor(Math.random() * (max - min)) + min;
	}
}

let gameElement = document.getElementById('game');

////////////////////////////////////////////kil select text inside main block
gameElement.onmousedown = function () {
	return false;
};
gameElement.onselectstart = function () {
	return false;
};
///////////////////////////////////////////

render(<Game/>, gameElement);
