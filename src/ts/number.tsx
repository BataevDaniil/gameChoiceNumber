import * as React from 'react';

export default class Number extends React.Component<PropsNumber,any> {

	constructor(props:any) {
		super(props);
	}

	public render() {
		const {num} = this.props;
		return (
			<div className={'number'} onClick={this.props.onClick}>
				{num}
			</div>
		)
	}
}
