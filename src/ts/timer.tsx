export default class Timer {
	public timerInterval:any;

	constructor(
		public time: SMH,
		public call: (time: SMH) => void,
		public revers = true) {
		this.start();
	}

	public removeTime(rmTime:SMH):SMH {
		this.time.second -= rmTime.second;
		this.time.minute -= rmTime.minute;
		this.time.hour -= rmTime.hour;
		return this.time;
	}

	public pause() {
		clearInterval(this.timerInterval)
	}

	public start() {
		this.timerInterval = setInterval(this.revers?this.countUp : this.countDown, 1000);
	}

	public countUp = () => {
		this.call(this.time);
		this.time.second++;

		if (this.time.second === 60) {
			this.time.second = 0;
			this.time.minute++;
			if (this.time.minute === 60) {
				this.time.minute = 0;
				this.time.hour++;
			}
		}

	};

	public countDown = () => {
		if (this.time.second === 0 && this.time.minute === 0 && this.time.hour === 0) {
			this.pause();
			this.call({second: 0, minute: 0, hour: 0});
		}
		else {
			this.call(this.time);
			if (this.time.second === 0) {
				this.time.second = 59;
				if (this.time.minute === 0) {
					this.time.minute = 59;
					this.time.hour--;
				}
				else
					this.time.minute--;
			}
			else
				this.time.second--;
		}

	}
}
