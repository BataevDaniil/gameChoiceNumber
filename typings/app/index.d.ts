
declare interface SMH {
	second: number;
	minute: number;
	hour: number;
}


declare interface PropsEqual {
	onClick:(event?:any) => void;
}

declare interface PropsGamePanel {
	timer:string;
	score:number;
}

declare interface PropsNumber {
	onClick:(event?:any) => void;
	num:number;
}

declare interface StateGame {
	firstNum:number;
	secondNum:number;
	timer:string;
	score:number;
}
